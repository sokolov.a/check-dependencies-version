const shell = require('shelljs')
const packages = ['lodash', 'moment']

const errors = packages
  .map(pack => (shell.exec(`yarn outdated ${pack}`).code !== 0) ? `Error: ${pack} is outdated` : null)
  .filter(i => i)

if (errors.length) {
  errors.forEach(e => shell.echo(e))
  shell.exit(1)
}
